# diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve, diplomacy_eval, territory_occupied, being_supported, being_attacked, most_supported

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ["A", "Madrid"])
    def test_read1(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ["A", "Madrid", "Hold"])
    def test_read2(self):
        s = "A Madrid Support B\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ["A", "Madrid", "Support", "B"])
    def test_read3(self):
        s = "A Madrid Move London\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ["A", "Madrid", "Move", "London"])

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\n")
    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"], ["B", "[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"], ["B", "[dead]"], ["C", "London"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\n")
    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"], ["B", "[dead]"], ["C", "London"], ["D", "Austin"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD Austin\n")

    # -----
    # Territory_occupied
    # -----

    def test_territory_occupied(self):
        a = territory_occupied(["A"], "A", ["Madrid"])
        self.assertEqual(a, ["A"])
    def test_territory_occupied1(self):
        a = territory_occupied(["A", "B"], "A", ["Madrid", "London"])
        self.assertEqual(a, ["A"])
    def test_territory_occupied2(self):
        a = territory_occupied(["A", "B"], "B", ["London", "London"])
        self.assertEqual(a, ["A", "B"])
    def test_territory_occupied3(self):
        a = territory_occupied(["A", "B","C"], "B", ["London", "London", "Madrid"])
        self.assertEqual(a, ["A", "B"])
    
    # -----
    # being_supported
    # -----

    def test_being_supported(self):
        a = being_supported(["Madrid"], ["A"], [0], ["A"], [0])
        self.assertEqual(a, [0])
    def test_being_supported1(self):
        a = being_supported(["Madrid", "London"], ["A"], [0, "A"], ["A", "B"], [0,0])
        self.assertEqual(a, [1])
    def test_being_supported2(self):
        a = being_supported(["London", "London", "Madrid"], ["A", "B"], [0, 0, "B"], ["A", "B", "C"], [0, "London", 0])
        self.assertEqual(a, [0, 1])
    def test_being_supported3(self):
        a = being_supported(["Madrid", "London", "London", "Madrid"], ["A", "D"], [0, 0, "A", 0], ["A", "B", "C", "D"], [0, "London", 0, "Madrid"])
        self.assertEqual(a, [0, 0])
    
    # -----
    # most_supported
    # -----

    def test_most_supported(self):
        a = most_supported([0], ["A"])
        self.assertEqual(a, None)
    def test_most_supported1(self):
        a = most_supported([0,0], ["A","B"])
        self.assertEqual(a, None)
    def test_most_supported2(self):
        a = most_supported([0,1], ["A","B"])
        self.assertEqual(a, "B")
    def test_most_supported3(self):
        a = most_supported([0,1,1,0], ["A","B","C","D"])
        self.assertEqual(a, None)
    
    # -----
    # being_attacked
    # -----

    def test_being_attacked(self):
        a = being_attacked([0], "Madrid")
        self.assertEqual(a, False)
    def test_being_attacked1(self):
        a = being_attacked([0, "London"], "Madrid")
        self.assertEqual(a, False)
    def test_being_attacked2(self):
        a = being_attacked([0, "Madrid"], "Madrid")
        self.assertEqual(a, True)
    def test_being_attacked3(self):
        a = being_attacked(["Madrid", "London"], "Houston")
        self.assertEqual(a, False)
    
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
    def test_solve2(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid \nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    def test_solve5(self):
        r = StringIO("A Madrid Move Austin\nB Barcelona Move Madrid\nC London Support A\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Madrid\nC [dead]\nD [dead]\n")
    def test_solve6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
    def test_solve7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
    def test_solve8(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\n")
    def test_solve9(self):
        r = StringIO("A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Beijing\nE NewYork Move Tokyo\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE [dead]\n")
    def test_solve10(self):
        r = StringIO("A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Tokyo\nE NewYork Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE NewYork\n")
    def test_solve11(self):
        r = StringIO("A NewYork Hold\nB Boston Move NewYork\nC Vancouver Move NewYork\nD Perth Support B\nE Dublin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Perth\nE Dublin\n")
    def test_solve12(self):
        r = StringIO("A Paris Hold\nB London Support A\nC Madrid Move London\nD Tokyo Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    def test_solve13(self):
        r = StringIO("A Austin Hold\nB Beijing Hold\nC Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Beijing\nC Madrid\n")
    def test_solve14(self):
        r = StringIO("A Austin Move Paris\nB Barcelona Move Paris\nC Tokyo Move Barcelona\nD Seattle Move Tokyo\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\nD Tokyo\n")
    def test_solve15(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Hold\nC Lisbon Move Barcelona\nD Vienna Move Barcelona\nE Berlin Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Barcelona\nE Berlin\n")

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
