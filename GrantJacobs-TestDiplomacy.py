#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    def test_solve_1(self):
        r = StringIO('A Madrid Hold')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A Madrid\n')
    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB Madrid\nC London\n')
    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB [dead]\n')
    def test_solve_4(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
    def test_solve_5(self):
        r = StringIO('A Paris Hold\nB London Support A\n C Madrid Move London\nD Tokyo Move Paris')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
    def test_solve_6(self):
        r = StringIO('A Austin Hold\nB Beijing Hold\n C Calgary Hold\nD Dublin Hold\nE Edinburgh Hold\nF Frankfurt Hold')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A Austin\nB Beijing\nC Calgary\nD Dublin\nE Edinburgh\nF Frankfurt\n')
    def test_solve_7(self):
        r = StringIO('A NewYork Hold\nB Boston Move NewYork A\n C Toronto Move NewYork\nD Perth Support B\nE Dublin Support A')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB [dead]\nC [dead]\nD Perth\nE Dublin\n')
    def test_solve_8(self):
        r = StringIO('A Rome Hold\nB Athens Support A\nC Milan Move Rome')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A Rome\nB Athens\nC [dead]\n')
    def test_solve_9(self): #check this one
        r = StringIO('A Rome Move Milan\nB Athens Hold\nC Milan Support B')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB Athens\nC [dead]\n')
    def test_solve_10(self):
        r = StringIO('A Rome Support C\nB Athens Hold\nC Milan Move Athens')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),'A Rome\nB [dead]\nC Athens\n')
    # def test_solve_11(self): #Failure Case on Purpose
    #     r = StringIO('A Rome Support C\nB Athens Hold\nC Milan Move Athens')
    #     w = StringIO()
    #     diplomacy_solve(r,w)
    #     self.assertEqual(
    #         w.getvalue(),'A Athens\nB Rome\n C Rome\n')
 
# ----
# main
# ----

if __name__ == "__main__": 
    main()
