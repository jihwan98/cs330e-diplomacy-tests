#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # read
    # -----

    def test_read_1(self):
        self.assertEqual(
            diplomacy_read("A Madison Move Sacramento\n"), ["A","Madison","Move","Sacramento"])
    
    def test_read_2(self):
        self.assertEqual(
            diplomacy_read("C Boston Support B\n"), ["C","Boston","Support","B"])

    def test_read_3(self):
        self.assertEqual(
            diplomacy_read("B Katy Hold\n"), ["B","Katy","Hold"])

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Austin Move Katy\nD Washington Move Beijing\nC Beijing Move Barcelona\nB Barcelona Move Katy\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\nD Beijing\n")

    def test_solve_2(self):
        r = StringIO("A Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Paris\n")

    def test_solve_3(self):
        r = StringIO("A London Hold\nB Seattle Support A\nC Tokyo Move Seattle\nD Houston Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_4(self):
        r = StringIO("A London Hold\nB Seattle Support A\nC Germany Hold\nD Katy Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Seattle\nC Germany\nD Austin\n")

 
    def test_solve_5(self):
        r = StringIO("A London Hold\nB Paris Move London\nC Katy Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\nC Katy\n")
    
    def test_solve_6(self):
        r = StringIO("A London Hold\nB Paris Move London\nC Katy Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB [dead]\nC Katy\n")
   




    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {"A": "[dead]", "B": "London"})
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\n")
    
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {"A": "Paris"})
        self.assertEqual(
            w.getvalue(), "A Paris\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {"A": "[dead]", "B": "[dead]", "C": "[dead]"})
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()

