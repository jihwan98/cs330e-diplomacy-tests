#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # ----
    # read
    # ----

    def test_read1(self):
        s = "A .[}/() Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ["A", ".[}/()", "Hold"])
        
    def test_read2(self):
        s = "A Madrid Move A\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ["A", "Madrid", "Move", "A"])
        
    def test_read3(self):
        s = "A Madrid Support B\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ["A", "Madrid", "Support", "B"])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Hold"]])
        self.assertEqual(v, [["A", "Madrid"], ["B", "Barcelona"]])

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, [["A", "[dead]"], ["B", "[dead]"]])

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Move", "Barcelona"], ["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, [["A", "Barcelona"], ["B", "Madrid"]])

    def test_eval_4(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]])
        self.assertEqual(v, [["A", "[dead]"], ["B", "Madrid"], ["C", "London"]])

    def test_eval_5(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]])
        self.assertEqual(v, [["A", "[dead]"], ["B", "[dead]"], ["C", "[dead]"], ["D", "[dead]"]])
        
    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"],["B", "Barcelona"]])
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\n")

    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"],["B", "[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, [["A", ""]])
        self.assertEqual(w.getvalue(), "A \n")

    # -----
    # solve
    # -----
    
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\n")
    
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    

# ----
# main
# ----


if __name__ == "__main__":
    main()

